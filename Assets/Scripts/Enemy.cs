﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : PlatformScript {

    //public int Hp;

    protected void Start()
    {
        base.Start();
    }

    protected void Update()
    {
        base.Update();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (this.tag == "Bumper" && collision.gameObject.tag == "Player")
        {
            collision.rigidbody.velocity = new Vector2(0,0);
            collision.rigidbody.AddForce(Vector2.up * 25, ForceMode2D.Impulse);
            GameManager.Instance.SetMultiScore(true);
        }

        if (this.tag == "Hazard" && collision.gameObject.tag == "Player")
        {
            collision.rigidbody.velocity = new Vector2(0, 0);
            collision.rigidbody.AddForce(Vector2.up * 25, ForceMode2D.Impulse);
            GameManager.Instance.SetLife(GameManager.Instance.GetLife()-2);
        }

    }
}
