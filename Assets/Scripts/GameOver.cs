﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour {

    [SerializeField] GameObject[] btns;
    [SerializeField] TMPro.TextMeshProUGUI score;

	// Use this for initialization
	void Start () {
        EventSystem.current.SetSelectedGameObject(btns[0]);
        score.text = "SCORE: " + Utils.Itos(GameManager.Instance.score, 9);
        GameObject.Find("Main Camera").GetComponent<Camera>().backgroundColor = GameManager.Instance.currentBckColor;
	}
	
	public void GoToMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void PlayAgain()
    {
        SceneManager.LoadScene(1);
    }
}
