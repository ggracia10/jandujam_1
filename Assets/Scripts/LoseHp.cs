﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoseHp : MonoBehaviour {

    [SerializeField] Sprite[] sprites;

	// Use this for initialization
	void Start () {
        ChangeFrame(6);
	}


    public void ChangeFrame(int _life)
    {
        gameObject.GetComponent<SpriteRenderer>().sprite = sprites[_life];
    }
}
