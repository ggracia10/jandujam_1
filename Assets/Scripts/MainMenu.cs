﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    public GameObject[] btns;

	// Use this for initialization
	void Start () {
        EventSystem.current.SetSelectedGameObject(btns[0]);
	}
	
	public void StartGame()
    {
        SceneManager.LoadScene(1);
    }

    public void Credits()
    {
        GameObject.Find("MadeBy").GetComponent<TMPro.TextMeshProUGUI>().enabled = true;
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
