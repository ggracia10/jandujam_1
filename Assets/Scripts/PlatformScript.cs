﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformScript : MonoBehaviour {
    
    public int endX;

    public void InitPlatform(int _endX)
    {
        endX = _endX;
    }

	// Use this for initialization
	protected void Start () {
		
	}
	
	// Update is called once per frame
	protected void Update () {
        transform.position += Vector3.left * GameManager.Instance.GetFinalSpeed() * Time.deltaTime;

        if (transform.position.x <= endX)
            Destroy(gameObject);
	}
}
