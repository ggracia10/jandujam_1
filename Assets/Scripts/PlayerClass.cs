﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerClass : MonoBehaviour
{


    public float speedJump, speed;

    public bool jumping, doubleJump, falling;

    public bool inFloor;

    Rigidbody2D rb;

    [SerializeField] GameObject Sprite;

    public float currentTimeScale, offsetTimeScale;

    float inputMovementX;

    Vector3 spriteRot;



    // Use this for initialization
    void Start()
    {
        GameManager.Instance.Init_GameManager();
        GameManager.Instance.speed = speed;
        currentTimeScale = 1;
        rb = GetComponent<Rigidbody2D>();
        falling = true;
        jumping = doubleJump = false;
        spriteRot = Vector3.zero;
    }

    // Update is called once per frame
    void Update()
    {
        UpdateControlls();
        if (doubleJump)
        {
            spriteRot.z -= Time.deltaTime * GameManager.Instance.speed * 50;
            Sprite.transform.rotation = Quaternion.Euler(spriteRot);
        }
            
    }

    private void FixedUpdate()
    {
        rb.velocity = new Vector2(inputMovementX * speed, rb.velocity.y);
    }


    void UpdateControlls()
    {
        if (InputManager.Instance.GetButtonDown("Action"))
        {
            Jump();
        }

        if (InputManager.Instance.GetButtonUp("Action"))
        {
            CancelJump();
        }

        inputMovementX = InputManager.Instance.GetAxis("MoveRight");
    }

    void Jump()
    {
        if (!jumping && !doubleJump && !falling)
        {
            jumping = true;
            doubleJump = false;
            rb.velocity = new Vector2(rb.velocity.x, 0);
            rb.AddForce(Vector2.up * speedJump, ForceMode2D.Impulse);
        }
        else if ((jumping || falling) && !doubleJump && GameManager.Instance.GetLife() > 0)
        {
            jumping = true;
            doubleJump = true;
            GameManager.Instance.SetLife(GameManager.Instance.GetLife()-1);
            rb.velocity = new Vector2(rb.velocity.x, 0);
            rb.AddForce(Vector2.up * speedJump, ForceMode2D.Impulse);
          
        }

    }
    void CancelJump()
    {
        rb.velocity = new Vector2(rb.velocity.x, -4.6f);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            jumping = false;
            doubleJump = false;
            falling = false;
            spriteRot = Vector3.zero;
            Sprite.transform.localRotation = Quaternion.Euler(Vector3.zero);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "LimitEdges")
        {
            GameManager.Instance.EndGame();
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Point")
        {
            jumping = false;
            doubleJump = false;
            falling = false;
            spriteRot = Vector3.zero;
            Sprite.transform.localRotation = Quaternion.Euler(Vector3.zero);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Point")
        {
            Debug.Log("Joder");
            jumping = true;
            doubleJump = false;
            falling = false;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ground" && !jumping)
        {
            jumping = false;
            doubleJump = false;
            falling = true;
        }
    }
}
