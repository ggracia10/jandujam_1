﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomPlat : MonoBehaviour {


    int Note = 1;
    int Note2;
    public int Type5Count = 0;

    public int iniX, endX;
    public float timeSpawn;

    public float globalTime;

    // Use this for initialization
    void Start()
    {
        StartCoroutine(generator());
    }

    // Update is called once per frame
    void Update()
    {
        GameManager.Instance.SetScore(Mathf.CeilToInt(Time.deltaTime * 25));
    }

    
    IEnumerator generator()
    {
        while (true)
        {
            yield return new WaitForSecondsRealtime(timeSpawn);

            Note2 = Note;
            switch (Note2)
            {
                case 1:
                    Note = Random.Range(1, 4);
                    Note2 = Note;
                    break;

                case 2:
                    Note = Random.Range(1, 5);
                    Note2 = Note;
                    break;

                case 3:
                    Note = Random.Range(1, 6);
                    Note2 = Note;
                    break;

                case 4:
                    Note = Random.Range(1, 6);
                    Note2 = Note;
                    break;

                case 5:
                    Note = Random.Range(1, 6);
                    Note2 = Note;
                    break;

                default:
                    break;
            }


            //Note = Random.Range(1, 6);
            GenerateNote(Note);
            //Debug.Log(Note);
        }
    }

    void GenerateNote(int iniY)
    {
        int type = Random.Range(0,7);
        if (type == 5)        
            Type5Count++;        
        else
            Type5Count = 0;

        if (Type5Count == 3)
            type = 4;

        GameObject newPlatform = Instantiate((GameObject)Resources.Load("Platforms/Platform" + type));
        if(type == 0 || type == 1 || type == 2)
        {
            newPlatform.GetComponent<SpriteRenderer>().color = GameManager.Instance.currentSqColor;
        }
        newPlatform.transform.position = new Vector3(iniX, iniY, 0);
        newPlatform.GetComponent<PlatformScript>().InitPlatform(endX);


        /*GameObject newNote = Instantiate(NoteColor[Color - 1], NoteColor[Color - 1].transform.position, Quaternion.identity) as GameObject;
        newNote.transform.SetParent(parent);
        StartCoroutine(MoveToPosition(newNote.transform, NoteTargets[Color - 1].transform.position, 2f));*/
    }

    ////float t;
    ////Vector3 startPos;
    ////Vector3 targetPos;
    ////float timeMove;

    //public void SetDestination(Vector3 destination, float time)
    //{
    //    t = 0;
    //    startPos = transform.position;
    //    timeMove = time;
    //    targetPos = destination;
    //}


    public IEnumerator MoveToPosition(Transform transform, Vector3 position, float timeToMove)
    {
        var currentPos = transform.position;
        var t = 0f;
        while (t < timeSpawn)
        {
            t += Time.deltaTime / timeToMove;
            transform.position = Vector3.Lerp(currentPos, position, t);
            yield return null;
        }
    }     
}
