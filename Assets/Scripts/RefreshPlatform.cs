﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RefreshPlatform : PlatformScript {

    bool phaseChanged;

	// Use this for initialization
	protected void Start () {
        base.Start();
        phaseChanged = false;
	}
	
	// Update is called once per frame
	protected void Update () {
        base.Update();
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!phaseChanged)
            GameManager.Instance.NextPhase(GameManager.Instance.currentPhase + 1);
        phaseChanged = true;
    }
}
