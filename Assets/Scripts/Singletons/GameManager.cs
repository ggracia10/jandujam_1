﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager: Singleton<GameManager>  {

    GameObject rewiredGO;

    public float speed;

    public int score;

    public int currentPhase;

    public int multiScore;

    public int life, lifeMax;

    LoseHp hud;

    TMPro.TextMeshProUGUI scoreTxt;

    TMPro.TextMeshProUGUI multiTxt;

    CameraFX cameraFx;

    Color currentColor;

    MusicManager musicManager;

    public Color currentBckColor, currentSqColor;



    public void SetMultiScore(bool up)
    {
        if (!up) multiScore = 1;
        else
        {
            multiScore *= 2;
        }
        multiTxt.text = "x" + multiScore.ToString();
    }

    public void NextPhase(int _phase)
    {
        currentPhase = _phase;
        SetLife(life + 1);
        SetSpeed();
    }

    public void SetSpeed()
    {
        speed = 11 + currentPhase * 1.5f;
    }


    public float GetFinalSpeed()
    {
        return speed;
    }

    public void Init_GameManager()
    {
        if(rewiredGO == null)
            rewiredGO = Instantiate((GameObject)Resources.Load("Rewired Input Manager"));
        currentPhase = -1;
        scoreTxt = GameObject.Find("ScoreTxt").GetComponent<TMPro.TextMeshProUGUI>();
        multiTxt = GameObject.Find("MultiTxt").GetComponent<TMPro.TextMeshProUGUI>();
        cameraFx = GameObject.Find("Main Camera").GetComponent<CameraFX>();
        hud = GameObject.Find("LoseHpAnimation").GetComponent<LoseHp>();
        multiScore = 1;
        score = 0;
        life = 6;
        musicManager = GameObject.Find("Music").GetComponent<MusicManager>();
    }

    public void SetScore(int _score)
    {
        if (score < 3500 && score + _score * multiScore >= 3500)
            musicManager.ChangeMusicPhase(1);
        else if (score < 15000 && score + _score * multiScore >= 15000)
            musicManager.ChangeMusicPhase(2);
        else if (score < 30000 && score + _score * multiScore >= 30000)
            musicManager.ChangeMusicPhase(3);

        score += _score * multiScore;
        scoreTxt.text = Utils.Itos(score, 9);
        
    }

    public void SetLife(int _life)
    {
        if (life > _life)
        {
            SetMultiScore(false);
            cameraFx.SetShaking(true, 0.1f);
        }
        life = _life;
        if (life > 6) life = 6;
        if (life < 0) EndGame();
        hud.ChangeFrame(life);
    }
    public int GetLife()
    {
        return life;
    }

    

    public void EndGame()
    {
        SceneManager.LoadScene(2);
        life = 0;
    }
}



