﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class InputManager : Singleton<InputManager> {

    int playerID;
    Player rPlayer;

    float axisX, axisY, axisXUp, axisYUp;

    // Use this for initialization
    public void Init_InputManager()
    {
        playerID = 0;
        rPlayer = ReInput.players.GetPlayer(playerID);
    }

    public float GetAxis(string axis)
    {
        return rPlayer.GetAxis(axis);
    }

    public bool GetAxisUp(string axis)
    {

        if (axis == "MoveRight" || axis == "UIHorizontal")
        {
            if (axisXUp > 0 && rPlayer.GetAxis(axis) < axisXUp && rPlayer.GetAxis(axis) >= 0 ||
                axisXUp < 0 && rPlayer.GetAxis(axis) > axisXUp && rPlayer.GetAxis(axis) <= 0)
            {
                axisXUp = rPlayer.GetAxis("MoveRight");
                axisX = 0;
                return true;
            }
            axisXUp = rPlayer.GetAxis("MoveRight");
        }
        else if (axis == "MoveForward" || axis == "UIVertical")
        {
            if (axisYUp > 0 && rPlayer.GetAxis(axis) < axisYUp && rPlayer.GetAxis(axis) >= 0 ||
                axisYUp < 0 && rPlayer.GetAxis(axis) > axisYUp && rPlayer.GetAxis(axis) <= 0)
            {
                axisYUp = rPlayer.GetAxis("MoveForward");
                axisY = 0;
                return true;
            }
            axisYUp = rPlayer.GetAxis("MoveForward");
        }

        return false;
    }

    public int GetAxisDown(string axis)
    {
        if (axis == "MoveRight" ||axis == "UIHorizontal")
        {
            if (axisX == 0 && rPlayer.GetAxis(axis) != 0)
            {
                axisX = rPlayer.GetAxis("MoveRight");
                if (axisX > 0) return 1;
                else return -1;
                
            }
            axisX = rPlayer.GetAxis("MoveRight");
        }
        else if(axis == "MoveForward" || axis == "UIVertical")
        {
            if (axisY == 0 && rPlayer.GetAxis(axis) != 0)
            {
                axisY = rPlayer.GetAxis("MoveForward");
                if (axisY > 0) return 1;
                else return -1;
            }
            axisY = rPlayer.GetAxis("MoveForward");
        }
        return 0;
    }

    public int GetFullAxis(string axis)
    {
        if (rPlayer.GetAxis(axis) > 0.1f) return 1;
        else if (rPlayer.GetAxis(axis) < -0.1f) return -1;
        else return 0;
    }

    public bool GetAnyButtonDown()
    {
        return rPlayer.GetAnyButtonDown();
    }

    public bool GetButtonDown(string btn)
    {
        return rPlayer.GetButtonDown(btn);
    }

    public bool GetButtonUp(string btn)
    {
        return rPlayer.GetButtonUp(btn);
    }

    public bool GetButton(string btn)
    {
        return rPlayer.GetButton(btn);
    }
}
