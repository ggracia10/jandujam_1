﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFX : MonoBehaviour {

    Vector3 currentShake, iniPos;
    bool shaking;
    float multShake;
    float timeShaking;

	// Use this for initialization
	void Start () {
        iniPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        if (timeShaking > 0)
        {
            timeShaking -= Time.deltaTime;
            if (timeShaking <= 0)
            {
                shaking = false;
                transform.position = iniPos;
            }
        }

        if (shaking)
            ShakeCamera();
    }

    void ShakeCamera()
    {
        currentShake.x = Random.Range(-1f, 1f);
        currentShake.y = Random.Range(-4f, 6f);
        currentShake.z = 0;
        transform.position =  iniPos + currentShake * multShake;
    }

    public void SetShaking(bool _shake, float _mult)
    {
        shaking = _shake;
        multShake = _mult;
        timeShaking = 0.5f;
    }

}
