﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitSingletons : MonoBehaviour {
    
	// Use this for initialization of Singletons
	void Awake () {
        if (GameObject.Find("(singleton) GameManager") != null) return;

        GameManager.Instance.Init_GameManager();
        InputManager.Instance.Init_InputManager();
    }

    private void Start()
    {
        Instantiate((GameObject)Resources.Load("Rewired Event System"));
    }
}
