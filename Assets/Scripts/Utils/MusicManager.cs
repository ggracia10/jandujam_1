﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour {

    [SerializeField] AudioSource [] music;

    AudioSource currentSong;

    [SerializeField] Color[] colorsBck;
    [SerializeField] Color[] colorsSq;
    

    public int currentMusicPhase;

	// Use this for initialization
	void Start () {
        currentMusicPhase = 0;
        currentSong = music[0];
        GameManager.Instance.currentBckColor = colorsBck[0];
        GameManager.Instance.currentSqColor = colorsSq[0];
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ChangeMusicPhase(int phase) {
        currentMusicPhase = phase;
        
        StartCoroutine(ChangeSong());
    }

    IEnumerator ChangeSong()
    {
        music[currentMusicPhase].Play();
        music[currentMusicPhase].volume = 0;
        while (currentSong.volume > 0)
        {
            GetComponent<SpriteRenderer>().color = 
                Color.Lerp(GameManager.Instance.currentBckColor, colorsBck[currentMusicPhase], music[currentMusicPhase].volume);
            currentSong.volume -= Time.deltaTime;
            music[currentMusicPhase].volume += Time.deltaTime;
            yield return null;
        }
        currentSong.Stop();
        currentSong = music[currentMusicPhase];
        GameManager.Instance.currentBckColor = colorsBck[currentMusicPhase];
        GameManager.Instance.currentSqColor = colorsSq[currentMusicPhase];

        GameObject.Find("Player").GetComponentInChildren<SpriteRenderer>().color = colorsSq[currentMusicPhase];
    }

}
