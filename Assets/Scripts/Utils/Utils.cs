﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utils  {

    //!Enum with directions (N=Nord, S=South, W=West, E=East)
    public enum Directions { N, NE, NW, S, SE, SW, E, W };

    //!Convert a Direction to a Vector
    public static Vector3 DirectionToVector(Directions dir)
    {
        Vector3 direction = Vector3.zero;
        switch (dir)
        {
            case Directions.N:
                direction = new Vector3(0, 0, 1);
                break;
            case Directions.S:
                direction = new Vector3(0, 0, -1);
                break;
            case Directions.E:
                direction = new Vector3(1, 0, 0);
                break;
            case Directions.W:
                direction = new Vector3(-1, 0, 0);
                break;
            case Directions.NE:
                direction = new Vector3(Mathf.Cos(DegToRad(45)), 0, Mathf.Cos(DegToRad(45)));
                break;
            case Directions.NW:
                direction = new Vector3(-Mathf.Cos(DegToRad(45)), 0, Mathf.Cos(DegToRad(45)));
                break;
            case Directions.SE:
                direction = new Vector3(Mathf.Cos(DegToRad(45)), 0, -Mathf.Cos(DegToRad(45)));
                break;
            case Directions.SW:
                direction = new Vector3(-Mathf.Cos(DegToRad(45)), 0, -Mathf.Cos(DegToRad(45)));
                break;
        }
        return direction;
    }

    //!Convert a Direction to an Angle
    public static float DirectionToAngle(Directions dir)
    {
        float angle = 0;
        switch(dir)
        {
            case Directions.E:
                angle = 0;
                break;
            case Directions.NE:
                angle = 45;
                break;
            case Directions.N:
                angle = 90;
                break;
            case Directions.NW:
                angle = 135;
                break;
            case Directions.W:
                angle = 180;
                break;
            case Directions.SW:
                angle = -135;
                break;
            case Directions.S:
                angle = -90;
                break;
            case Directions.SE:
                angle = -45;
                break;
        }
        return angle;
    }

    //!Convert an Angle to a Direction
    public static Directions AngleToDirection(float angle)
    {
        Directions currentDirection = Directions.S;
        if (angle > -22.5F && angle < 22.5F)
            currentDirection = Utils.Directions.E;
        else if (angle >= 22.5F && angle <= 67.5)
            currentDirection = Utils.Directions.NE;
        else if (angle > 67.5F && angle < 112.5F)
            currentDirection = Utils.Directions.N;
        else if (angle >= 112.5F && angle <= 157.5F)
            currentDirection = Utils.Directions.NW;
        else if (angle > 157.5F && angle <= 180F ||
            angle < -157.5F && angle >= -180F)
            currentDirection = Utils.Directions.W;
        else if (angle >= -157.5F && angle <= -112.5F)
            currentDirection = Utils.Directions.SW;
        else if (angle > -112.5F && angle < -67.5F)
            currentDirection = Utils.Directions.S;
        else if (angle >= -67 && angle <= -22.5F)
            currentDirection = Utils.Directions.SE;
        return currentDirection;
    }

    //!Function to transform Degrees to Radians
    public static float DegToRad(float deg)
    {
        return deg * Mathf.PI / 180;
    }

    //!Function to transform Radians to Degrees
    public static float RadToDeg(float rad)
    {
        return rad * 180 / Mathf.PI;
    }

    //!Function to calculate the module of Vector on XZ
    public static float ModuleOfAVectorXZ(Vector3 newVect)
    {
        return Mathf.Sqrt((Mathf.Pow(newVect.x, 2) + Mathf.Pow(newVect.z, 2)));
    }
    //!Function to calculate the module of Vector on XY
    public static float ModuleOfAVectorXY(Vector3 newVect)
    {
        return Mathf.Sqrt((Mathf.Pow(newVect.x, 2) + Mathf.Pow(newVect.y, 2)));
    }
    //!Function to calculate the module of Vector on YZ
    public static float ModuleOfAVectorYZ(Vector3 newVect)
    {
        return Mathf.Sqrt((Mathf.Pow(newVect.y, 2) + Mathf.Pow(newVect.z, 2)));
    }
    //!Function to calculate the module of Vector on XYZ
    public static float ModuleOfAVectorXYZ(Vector3 newVect)
    {
        return Mathf.Sqrt((Mathf.Pow(newVect.x, 2) + Mathf.Pow(newVect.y, 2) + Mathf.Pow(newVect.z, 2)));
    }

    //!Function to calculate the 3 angles of a Vector
    public static Vector3 AnglesOfAVector(Vector3 newVect, float moduleVect)
    {
        return (new Vector3(Mathf.Acos(newVect.x / moduleVect), Mathf.Acos(newVect.y / moduleVect),
            Mathf.Acos(newVect.z / moduleVect)));
    }
    //!Returns the angle of a 2D vector
    public static float AngleOfAVector2D(float x, float y)
    {
        return (Mathf.Atan2(y, x));
    }

    //!Function to calculate the angle between 2 vectors
    public static float AngleOf2Vectors(Vector3 vect1, Vector3 vect2)
    {
        float newAngle = Mathf.Acos(Mathf.Abs(vect1.x * vect2.x + vect1.y * vect2.y + vect1.z + vect2.z) /
            (ModuleOfAVectorXYZ(vect1) * ModuleOfAVectorXYZ(vect2)));

        Debug.Log(RadToDeg(newAngle));

        return newAngle;
    }

    //!Function to calculaate the max number of 2 numbers
    public static float MaxNumber (float x, float y)
    {
        float maxNum = 0;
        if (x > y) maxNum = x;
        else maxNum = y;
        return maxNum;
    }

    //!Function to calculaate the min number of 2 numbers
    public static float MinNumber(float x, float y)
    {
        float minNum = 0;
        if (x > y) minNum = y;
        else minNum = x;
        return minNum;
    }

    //! Cast int to string
    public static string Itos(int num, int cif)
    {
        string ret = "";
        for (int i = 0; i < cif; i++)
        {
            num = num % (int)Mathf.Pow(10, cif - i);

            if (num / Mathf.Pow(10, cif - i - 1) < 1)
                ret += "0";
            else ret += ((int)(num / Mathf.Pow(10, cif - i - 1))).ToString();
        }
        return ret;
    }

    //!Cast String to Int
    public static int Stoi(string num)
    {
        int number = 0;
        for(int i = 0; i < num.Length; i++)
        {
            number += ((int)num[i] - 48) * (int)Mathf.Pow(10, num.Length -1 - i);
        }
        return number;
    }

    public static int ByteSystemToInt(int[] x)
    {
        if (x[0] == 255) x[0] = -1; //it's a byte, so 0-1 = 255 because it doesn't exists negative numbers
        return x[0] * (x[1] * 100 + x[2]); //this returns the number that we want
    }

    //To cast a int to 3 UINT8 (from -9999 to +9999)
    //(the first will be the sign: -1 / 1)
    //(the second will be the first 2 numbers)
    //(the third one will be the 3rd and 4th numbers of the int)
    public static int[] LargeIntToByteSystem(int x)
    {
        int[] ret = new int[3];
        string numS = x.ToString();
        int size = numS.Length;
        //Cast the number x to a string like this: x = 79 -> numS = "+0079" | x = -851 -> numS = "-0851"
        if (numS[0] == '-')
        {
            numS = "-";
            for (int i = 0; i < 4 - size + 1; i++)
            {
                numS += '0'; //We add as 0 at the beggin of the string
            }
            numS += (x.ToString().Substring(1, x.ToString().Length - 1)); //Then we add the number without the sign
        }
        else
        {
            numS = "+";
            for (int i = 0; i < 4 - size; i++)
            {
                numS += '0';
            }
            numS += x.ToString();

        }
        //Then we cast this string to 3 ints
        if (numS[0] == '-') ret[0] = -1;
        else ret[0] = 1;
        string temp = "" + numS[1] + numS[2];
        ret[1] = Utils.Stoi("" + numS[1] + numS[2]);
        ret[2] = Utils.Stoi("" + numS[3] + numS[4]);

        return ret;
    }

}
